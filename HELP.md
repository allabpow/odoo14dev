
# help

```shell
# start Odoo:
cd /var/www/html/odoo-dev
odoo/odoo-bin -c myodoo.cfg
# https://www.odoo.com/documentation/15.0/developer/howtos/rdtraining/04_basicmodel.html
# start Odoo and update module:
odoo/odoo-bin -c myodoo.cfg -u my_library
```

[think:8069](http://think:8069)


## odoo.com/documentation & github.com/odoo
[Developer Mode (debug mode)](https://www.odoo.com/documentation/15.0/applications/general/developer_mode.html)

module category names: [github.com/odoo/odoo](https://github.com/odoo/odoo/blob/15.0/odoo/addons/base/data/ir_module_category_data.xml)

# experience

The file `my_library/static/description/icon.png` can not stand alone in that folder.
...always it throws an error without an `index.html` in that folder.
It is enough if the file is in there once with a running Odoo.
Without an `index.html` the description is taken from `__manifest__.py`.