from odoo import models


class BtkrnEstate(models.Model):
    _name = "btkrn.estate"
    _description = "The Bitkorn Estate model."
