# -*- coding: utf-8 -*-

{
    'name': 'Estates',
    'version': '0.0.1',
    'author': "Torsten Brieskorn",
    'depends': ['base'],
    'category': 'Productivity',
    'summary': 'Sell estates',
    'description': "With this module, you can sell estates.",
    'website': "http://bitkorn.de",
    'license': "LGPL-3",
}
